

export default async function (configData) {
  const data = {
    ...configData,
    lang: 'en',
  }
  const result = {}
  try {
  const response = await fetch(`https://cloud.squidex.io/api/content/silex-me/graphql`, {

  headers: {
    'content-type': `application/json`,
'X-Languages': `${data.lang}`,
'Authorization': `Bearer eyJhbGciOiJSUzI1NiIsImtpZCI6IjlTZWo3eXJYdGo1TzNDVV9JeG5EU2ciLCJ0eXAiOiJhdCtqd3QifQ.eyJzdWIiOiJzaWxleC1tZTpzaWxleDExdHkiLCJvaV9wcnN0Ijoic2lsZXgtbWU6c2lsZXgxMXR5IiwiY2xpZW50X2lkIjoic2lsZXgtbWU6c2lsZXgxMXR5Iiwib2lfdGtuX2lkIjoiOWE5YjdmNzEtZDJjOC00NjJjLWE2MWUtMTMzMTJiZjBmODQxIiwiYXVkIjoic2NwOnNxdWlkZXgtYXBpIiwic2NvcGUiOiJzcXVpZGV4LWFwaSIsImp0aSI6IjljNWYxM2ZjLWJlYzktNGE5YS04MTJiLTY2YWI5ZmI4MzYyMiIsImV4cCI6MTcwNjQzOTA4MCwiaXNzIjoiaHR0cHM6Ly9jbG91ZC5zcXVpZGV4LmlvLyIsImlhdCI6MTcwMzg0NzA4MH0.VqUUgNjmFb1J6gStqvT0uB0zs3VrpP92_7-S6bKafOX_-BdFEZPw1R2YKH5PuB-X8MXiSNxRpjPsRLRg-6JJ5J2F_5hoIOW8dZkNtwA3MEy8j2tdwAWorPDs4HRkpISQSPCBYiz3L5gnipMMEkpIT_5nfsaztDd6yK0HY28RwfIACdmp9PlTQ9ZQaCyIkPxZh6yAGCpcF2d-2KDxxY0vZ7YFrV4eMzqUxVhvQ07ouGzE15y-K3cfO2I3nmQg5ULQGXmpI4SoXtAptIpLwSjeFJzh2KczQ7XQQU04EO7jh7ceoUjhxvvxhurE_OTh3d3ovAxIPdfvJhv_29wCfTAIiQ`,
  },
  method: 'POST',
  body: JSON.stringify({
    query: `query {
__typename
queryPageContents(skip: 0) {
  __typename
  data {
    __typename
    slug {
      __typename
      fr
      en

    }
    modules {
      __typename
      en {
        __typename
        item__Dynamic

      }

    }

  }
  flatData {
    __typename
    shareImage {
      __typename
      url

    }
    description
    lang
    slugFr
    slugEn
    slug
    listImage {
      __typename
      url

    }
    title
    pubDate
    type
    modules {
      __typename

    item {
        ...on HeroWordSliderComponent {
        __typename
        type
        before
        words {
          __typename
          word

        }
        after
        cTAUrl
        cTALabel

      }
}
    item {
        ...on SimpleHeroComponent {
        __typename
        type
        text
        secondaryUrl
        secondaryLabel
        cTAUrl
        cTALabel

      }
}
    item {
        ...on SectionSlideshowUpComponent {
        __typename
        type

      }
}
    item {
        ...on SectionImageUpComponent {
        __typename
        type
        image {
          __typename
          url
          fileName

        }

      }
}
    item {
        ...on SpecialHeadingComponent {
        __typename
        styleBg
        type
        title
        subtitle
        text
        uRLSecondary
        labelSecondary
        uRL
        label

      }
}
    item {
        ...on SectionPatternGridComponent {
        __typename
        type
        style
        title
        subtitle
        text
        uRLSecondary
        labelSecondary
        uRL
        label
        image {
          __typename
          url
          slug

        }
        shadow

      }
}
    item {
        ...on SectionCodeComponent {
        __typename
        type
        code

      }
}
    item {
        ...on SectionFriendsComponent {
        __typename
        type

      }
}
    item {
        ...on SectionSquaresComponent {
        __typename
        type
        styleBg
        mainTitle
        mainSubtitle
        mainText
        cards {
          __typename
          image {
            __typename
            url
            fileName

          }
          title
          text
          uRL
          label

        }
        cTAUrl
        cTALabel

      }
}
    item {
        ...on SectionSquaresFeaturesComponent {
        __typename
        type
        featureTitle
        featureSubtitle
        featureText

      }
}
    item {
        ...on SectionSquaresPagesComponent {
        __typename
        type
        filterByType

      }
}
    item {
        ...on SpecialHeadingImgComponent {
        __typename
        type
        image {
          __typename
          url
          slug

        }
        title
        subtitle
        text
        uRLSecondary
        labelSecondary
        uRL
        label

      }
}
    item {
        ...on SectionRectBgComponent {
        __typename
        type
        title
        subtitle
        cards {
          __typename
          link
          image {
            __typename
            url
            fileName

          }
          title
          text

        }

      }
}
    item {
        ...on SectionNetworkComponent {
        __typename
        type
        title
        subtitle
        cards {
          __typename
          link
          image {
            __typename
            url
            fileName

          }
          title
          text

        }

      }
}
    item {
        ...on SectionBlogDataComponent {
        __typename
        type
        author {
          __typename
          flatData {
            __typename
            name

          }

        }
        date

      }
}
    }

  }

}
findGlobalSingleton {
  __typename
  flatData {
    __typename
    favicon {
      __typename
      url

    }
    url
    nav {
      __typename
      label
      url
      title

    }
    footerHeadingsTitle
    footerHeadingsSubtitle
    footerHeadingsText
    uRLSecondary
    labelSecondary
    uRL
    label
    links {
      __typename
      title
      content

    }
    footerLegal

  }

}
queryFeatureContents(skip: 0) {
  __typename
  flatData {
    __typename
    image {
      __typename
      url
      fileName

    }
    title
    text
    uRL
    label

  }

}
queryGlobalContents(skip: 0) {
  __typename
  flatData {
    __typename
    ctaCardsLabel

  }

}

}`,
  })
  })

  if (!response.ok) {
    throw new Error(`Error fetching graphql data: HTTP status code ${response.status}, HTTP status text: ${response.statusText}`)
  }

  const json = await response.json()

  if (json.errors) {
    throw new Error(`GraphQL error: \
> ${json.errors.map(e => e.message).join('\
> ')}`)
  }

  result['squidex'] = json.data
} catch (e) {
  console.error('11ty plugin for Silex: error fetching graphql data', e, 'squidex', 'https://cloud.squidex.io/api/content/silex-me/graphql')
  throw e
}
  return result
}
  