export default async function () {
  const response = await fetch('https://libre-friends.silexlabs.org/api/projects.json')
  const projects = await response.json()
  return {
    projects,
  }
}